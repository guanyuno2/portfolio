apt-get update && apt-get install -y openssh-client

# Variables
TEMP_KEY_FILE=$(mktemp)
echo "$SSH_PRIVATE_KEY" > "$TEMP_KEY_FILE"
chmod 600 "$TEMP_KEY_FILE"  # Set appropriate permissions

# Start the SSH agent
eval "$(ssh-agent -s)"

# Add the private key to the SSH agent with the passphrase
echo "$SSH_PASSPHRASE" | ssh-add "$TEMP_KEY_FILE"

# SSH command
ssh -o StrictHostKeyChecking=no -i "$TEMP_KEY_FILE" root@"$IP_REMOTE_SERVER" "\
  docker login -u $REGISTRY_USER -p $REGISTRY_PASS && docker pull $IMAGE_FULL && docker run -d -p 5007:3000 --name $CONTAINER_NAME $IMAGE_FULL"

# Clean up
ssh-agent -k  # Stop the SSH agent
rm "$TEMP_KEY_FILE"  # Remove the temporary key file
