# Use the official Node.js image as the base image
FROM node:18.17.0

# Create and set the working directory
WORKDIR /usr/src/app

# Copy package.json and package-lock.json
COPY package*.json ./

# Install dependencies
RUN npm install

# Copy the rest of the application code
COPY . .

# Build the Next.js application
RUN npm run build

# Expose the port the application will run on
ENV PORT 3000
EXPOSE 3000

# Define the command to run the application
CMD ["npm", "start"]