import { Button } from "@/components/ui/button"
import { Card, CardContent } from "@/components/ui/card"
import { log } from "console";

export interface ProjectItem {
    title: string;
    description: string;
    imageUrl: string;
    link?: string
}

interface ProjectsProps {
    projects: ProjectItem[];
}

export function Projects({ projects }: ProjectsProps) {
    if (projects.length === 0) {
        return null;
    }

    const openNewWindow = (url: string | undefined) => {
        console.log("url", url);
        
        if(!url) return;
        window.open(url, "_blank");
    }

    return (
        <section id="projects" className="space-y-4">
            <h2 className="text-3xl font-bold">Projects</h2>
            <div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
                {projects.map((project, index) => (
                    <Card key={index}>
                        <CardContent className="p-4 space-y-4">
                            <img
                                src={project.imageUrl}
                                alt={project.title}
                                className="w-full h-40 object-cover rounded-md"
                            />
                            <h3 className="text-xl font-semibold">{project.title}</h3>
                            <p className="text-sm text-muted-foreground">
                                {project.description}
                            </p>
                            <Button variant="outline" className="w-full" onClick={() => openNewWindow(project.link)}>View Project</Button>
                        </CardContent>
                    </Card>
                ))}
            </div>
        </section>
    )
}
