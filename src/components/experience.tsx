import { Badge } from "@/components/ui/badge"

interface ExperienceItem {
    title: string;
    company: string;
    period: string;
    responsibilities: string[];
    skills: string[];
}

interface ExperienceProps {
    experiences: ExperienceItem[];
}

export function Experience({ experiences }: ExperienceProps) {
    return (
        <section id="experience" className="space-y-6">
            <h2 className="text-3xl font-bold">Experience</h2>
            {experiences.map((exp, index) => (
                <div key={index} className="space-y-2">
                    <h3 className="text-xl font-semibold">{exp.title}</h3>
                    <p className="text-primary">{exp.company} | {exp.period}</p>
                    <ul className="list-disc list-inside space-y-1 text-muted-foreground">
                        {exp.responsibilities.map((resp, idx) => (
                            <li key={idx}>{resp}</li>
                        ))}
                    </ul>
                    <div className="flex flex-wrap gap-2 mt-2">
                        {exp.skills.map((skill, idx) => (
                            <Badge key={idx} variant="outline" className="text-sm">
                                {skill}
                            </Badge>
                        ))}
                    </div>
                </div>
            ))}
        </section>
    )
}
