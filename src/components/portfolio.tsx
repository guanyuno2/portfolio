'use client'

import { Button } from "@/components/ui/button"
import { Card, CardContent } from "@/components/ui/card"
import { Badge } from "@/components/ui/badge"
import { Download, Github, Gitlab, Linkedin, Mail } from "lucide-react"
import { Experience } from "./experience"
import { ProjectItem, Projects } from "./projects"
// import Image from 'next/image';

const projects = [
	{
		title: "Identity Provider",
		description: "A custom identity provider built with .NET Core and MySQL. It provides authentication and authorization services for multiple applications.",
		imageUrl: "/images/sso.jpg?height=200&width=300&text=E-commerce+Platform",
		link: "https://gitlab.com/guanyuno2/sso"
	},
	{
		title: "File/Folder Management",
		description: "A file and folder management system built with ReactJS and C# using the BFF (Backend-for-Frontend) pattern. It allows users to upload, download, and manage files and folders.",
		imageUrl: "/placeholder.svg?height=200&width=300&text=Task+Management+App",
		link: "https://gitlab.com/guanyuno2/react-net-6"
	},
	{
		title: "File/Folder Management API",
		description: "A RESTful API built with .NET Core and MongoDB. It provides endpoints for managing files and folders.",
		imageUrl: "/placeholder.svg?height=200&width=300&text=Weather+Dashboard",
		link: "https://gitlab.com/guanyuno2/api"
	}
] as ProjectItem[]

const experiences = [
	{
		title: "Back-End Engineer",
		company: "Central Retail Vietnam",
		period: "2024 - Present",
		responsibilities: [
			"Implemented gRPC in this project by setting up both a gRPC Server and Client",
			"Created a centralized API service that integrates all in-house and vendor services",
			"Built a high-capacity service with extensive stress testing",
			"Set up a high-throughput webhook service to distribute messages to Kafka",
			"Implemented SonarQube and Unit test coverage to increase the quality of the code"
		],
		skills: ["C#", ".NET Core", "Kafka", "Unit Test", "gRPC", "RESTful APIs", "Webhook", "Performance Optimization"]
	},
	{
		title: "Senior Software Engineer",
		company: "HARAVAN",
		period: "2021 - 2023",
		responsibilities: [
			"Ability to plan resources and follow up on sprint progress",
			"Mainly responsible for the technical aspects of both the front-end and back-end of the product",
			"Designing databases and monitoring the system in real-time to ensure stability through alerts",
			"Managing versions and deploying for the production environment"
		],
		skills: ["ReactJS", "C#", ".NET Core", "Typescript", "RESTful APIs", "MongoDB", "Docker", "CI/CD", "HTML/CSS"]
	},
	{
		title: "Front-End Engineer",
		company: "HARAVAN",
		period: "2018 - 2020",
		responsibilities: [
			"Designing, developing, and implementing user interfaces for web applications or software products",
			"Optimizing front-end code and assets to improve page load times and overall performance of web applications"
		],
		skills: ["ReactJS", "Typescript", "HTML/CSS"]
	}
]

export function Portfolio() {
	const skills = [
		".Net Core", "C#",
		"React", "Next.js", "TypeScript", "JavaScript", "HTML", "CSS",
		"Tailwind CSS", "MongoDB", "Git", "REST APIs",
		"Docker", "CI/CD", "Kafka", "gRPC", "Unit Test", "Performance Optimization",
		"Webhook", "OAuth2"
	]

	return (
		<div className="min-h-screen bg-background text-foreground">
			<header className="sticky top-0 z-10 backdrop-blur-sm bg-background/90 border-b">
				<div className="container mx-auto px-4 py-4 flex justify-between items-center">
					<h1 className="text-2xl font-bold">Ty Van</h1>
					<nav>
						<ul className="flex space-x-4">
							<li><a href="#about" className="hover:text-primary transition-colors">About</a></li>
							<li><a href="#experience" className="hover:text-primary transition-colors">Experience</a></li>
							<li><a href="#skills" className="hover:text-primary transition-colors">Skills</a></li>
							<li><a href="#projects" className="hover:text-primary transition-colors">Projects</a></li>
							<li><a href="#contact" className="hover:text-primary transition-colors">Contact</a></li>
						</ul>
					</nav>
				</div>
			</header>

			<main className="container mx-auto px-4 py-8 space-y-16">
				<section id="about" className="space-y-4">
					<h2 className="text-3xl font-bold">About Me</h2>
					<div className="flex flex-col md:flex-row gap-8 items-center">
						<img
							src="/images/selfie.jpg?height=300&width=300"
							alt="Bui Van Ty"
							className="w-48 h-48 rounded-full object-cover"
						/>
						<div className="space-y-4">
							<p className="max-w-2xl">
								As someone who is always proactive at work, I will always find ways to solve problems, even if I have never encountered them before. I am extremely passionate about tasks that create significant value for the end-user.
							</p>
							<Button className="flex items-center space-x-2"
								onClick={() => {
									window.open('https://static.tyvan.dev/resume-update-1.pdf', '_blank')
								}}
							>
								<Download className="w-4 h-4" />
								<span>Download CV</span>
							</Button>
						</div>
					</div>
				</section>

				<Experience experiences={experiences} />

				{/* <section id="projects" className="space-y-4">
					<h2 className="text-3xl font-bold">Projects</h2>
					<div className="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6">
						{[1, 2, 3].map((project) => (
							<Card key={project}>
								<CardContent className="p-4 space-y-4">
									<img
										src={`/placeholder.svg?height=200&width=300&text=Project+${project}`}
										alt={`Project ${project}`}
										className="w-full h-40 object-cover rounded-md"
									/>
									<h3 className="text-xl font-semibold">Project {project}</h3>
									<p className="text-sm text-muted-foreground">
										A brief description of project {project} and the technologies used.
									</p>
									<Button variant="outline" className="w-full">View Project</Button>
								</CardContent>
							</Card>
						))}
					</div>
				</section> */}
				<Projects projects={projects} />

				<section id="skills" className="space-y-4">
					<h2 className="text-3xl font-bold">Skills</h2>
					<div className="flex flex-wrap gap-2">
						{skills.map((skill, index) => (
							<Badge key={index} variant="secondary" className="text-sm">
								{skill}
							</Badge>
						))}
					</div>
				</section>

				<section id="contact" className="space-y-4">
					<h2 className="text-3xl font-bold">Contact Me</h2>
					<div className="flex flex-wrap gap-4">
						<Button variant="outline" className="flex items-center space-x-2"
							asChild
						>
							<a href="mailto:bvanty96@gmail.com">
								<Mail className="w-4 h-4" />
								<span>Email</span>
							</a>
						</Button>
						<Button
							variant="outline"
							className="flex items-center space-x-2"
							asChild
						>
							<a href="https://gitlab.com/guanyuno2/" target="_blank" rel="noopener noreferrer">
								<Gitlab className="w-4 h-4" />
								<span>GitLab</span>
							</a>
						</Button>

						<Button
							variant="outline"
							className="flex items-center space-x-2"
							asChild
						>
							<a href="https://www.linkedin.com/in/iofty/" target="_blank" rel="noopener noreferrer">
								<Linkedin className="w-4 h-4" />
								<span>LinkedIn</span>
							</a>
						</Button>
					</div>
				</section>
			</main>

			<footer className="bg-muted py-4 mt-16">
				<div className="container mx-auto px-4 text-center text-sm text-muted-foreground">
					© 2024 Ty Van. All rights reserved.
				</div>
			</footer>
		</div>
	)
}